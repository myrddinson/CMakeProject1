
#include <stdio.h>
#include <xstring>

class VMyClass
{
public:
	VMyClass() {}
	virtual ~VMyClass() {}

	void SetText(const std::string& inText);
	const std::string& GetText() { return fText; }

protected:
	std::string fText;

};