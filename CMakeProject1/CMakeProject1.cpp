// CMakeProject1.cpp : définit le point d'entrée de l'application.
//

#include "CMakeProject1.h"
#include "VMyClass.h"

using namespace std;

int main()
{
	VMyClass* myclass = new VMyClass();
	myclass->SetText("toto");

	cout << "Hello CMake. comment ça va " << myclass->GetText() << "?" << endl;
	cout << "toto et titi montent dans un bateau";

	delete myclass;

	return 0;
}
